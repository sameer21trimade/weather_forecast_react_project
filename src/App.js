import React from 'react';
import './App.css';
import Axios from 'axios'
import DisplayWeatherInfo from './weatherAppComponents/DisplayWeatherInfo'

class App extends React.Component {
  constructor() {
    super()
    this.state = {
      location: { longitude: 0, latitude: 0 },
      weatherUpdate: {},
      locationGivenByUser: {}
    }
    this.onChangeEventHandler=this.onChangeEventHandler.bind(this)
    this.onSubmitEventHandler=this.onSubmitEventHandler.bind(this)
  }
  componentDidMount() {

        Axios.get(`https://geolocation-db.com/json/697de680-a737-11ea-9820-af05f4014d91`)
    .then((apiDataGeoLoc) => {  
      let longitudeLocation = apiDataGeoLoc.data.longitude
      let latitudeLocation = apiDataGeoLoc.data.latitude
      let currentLocation = { longitude: longitudeLocation, latitude: latitudeLocation }
      this.setState({ location: currentLocation }) 

      Axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${this.state.location.latitude}&lon=${this.state.location.longitude}&appid=c2a852be3cdc71a30fba3147668de5ee&units=metric`)
      .then((apiData) => {
        let weatherInfo = {
          temperature: apiData.data.main.temp,
          description: apiData.data.weather[0].description,
          location: apiData.data.name,
          country: apiData.data.sys.country,
          min_temp: apiData.data.main.temp_min,
          max_temp: apiData.data.main.temp_max,
          humidity: apiData.data.main.humidity,
          climateImage: "https://openweathermap.org/img/w/" + apiData.data.weather[0].icon + ".png",
          wind_speed: apiData.data.wind.speed,
          pressure: apiData.data.main.pressure
        }
        this.setState({ weatherUpdate: weatherInfo }) 
      })
    })
  }  
  onChangeEventHandler(event) {
    this.setState({locationGivenByUser:event.target.value})    
  }
  
  onSubmitEventHandler(event) {
    event.preventDefault()    
    Axios.get(`https://geolocation-db.com/json/697de680-a737-11ea-9820-af05f4014d91`)
    .then((apiDataGeoLoc) => {  
      let longitudeLocation = apiDataGeoLoc.data.longitude
      let latitudeLocation = apiDataGeoLoc.data.latitude
      let currentLocation = { longitude: longitudeLocation, latitude: latitudeLocation }
      this.setState({ location: currentLocation }) 
      Axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${this.state.locationGivenByUser}&appid=c2a852be3cdc71a30fba3147668de5ee&units=metric`)
      .then((apiData) => {
        console.log(apiData)
        let weatherInfo = {
          temperature: apiData.data.main.temp,
          description: apiData.data.weather[0].description,
          location: apiData.data.name,
          country: apiData.data.sys.country,
          min_temp: apiData.data.main.temp_min,
          max_temp: apiData.data.main.temp_max,
          humidity: apiData.data.main.humidity,
          climateImage: "https://openweathermap.org/img/w/" + apiData.data.weather[0].icon + ".png",
          wind_speed: apiData.data.wind.speed,
          pressure: apiData.data.main.pressure
        }
        this.setState({ weatherUpdate: weatherInfo }) 
      })
    })
  }
  render() {
    return (
      <DisplayWeatherInfo apiData={this.state.weatherUpdate} onSubmitEventHandler={this.onSubmitEventHandler} onChangeEventHandler={this.onChangeEventHandler} />
    )
  }
}

export default App
